# CarCar

Team:

* Wayne Basile - Service
* Michael Parnapy - Sales

## Design

CarCar follows the principles of Domain Driven Design by incorporating Microservices architecture and Bounded Contexts (each microservice). It also identifies value objects and integrates Inter-Processs communication to poll for the value objects between different microservices, which enables it to maintain good Data Stewardship.

## Service microservice

The service microservice polls for automobile data from the inventory microservice, which it then turns into value objects to handle creation of appointments and determine whether or not a serviced vehicle should be flagged for "VIP" status. The backend of the microservice uses RESTful APIs to handle HTTP requests to create, read, update, and delete data. It then uses React, Javascript, and Boostrap to render the data in the browser dynamically.

## Sales microservice

The sales app will impliment three api's to handle sales information. The customer api will allow the creation and deletion of a customer as well a listing of all customers. The salesperson api will allow the creation and deletion of a salesperson as well as a listing of all salespeople. The sales api will allow the creation of a sale depending on information from the customer api, the salesperson api and the automobile value object.
