import React, {useState, useEffect} from 'react'

function AutomobileForm() {

    const [vin, setVin] = useState('');
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [model, setModel] = useState('');
    const [models, setModels] = useState([]);
    const [hasSignedUp, setHasSignedUp] = useState(false);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.vin = vin;
        data.color = color;
        data.year = year;
        data.model_id = model;

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const automobileResponse = await fetch(automobileUrl, fetchOptions);
        if (automobileResponse.ok) {
            setVin('');
            setColor('');
            setYear('');
            setModel('');
            setHasSignedUp(true);
        }
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (models.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select border-success';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return(
        <div className='my-5 container'>
            <div className='row'>
                <div className='col'></div>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <form className={formClasses} onSubmit={handleSubmit} id='create-automobile-form'>
                                <h1 className='card-title'>Add an automobile</h1>
                                <div className='col'>
                                    <div className='form-floating mb-3'>
                                        <input onChange={handleVinChange} required placeholder='VIN' type='text' id='vin' name='vin' className='form-control' />
                                        <label htmlFor=''>VIN...</label>
                                    </div>
                                </div>

                                <div className='col'>
                                    <div className='form-floating mb-3'>
                                        <input onChange={handleColorChange} required placeholder='Color' type='text' id='color' name='color' className='form-control' />
                                        <label htmlFor='color'>Color...</label>
                                    </div>
                                </div>

                                <div className='col'>
                                    <div className='form-floating mb-3'>
                                        <input onChange={handleYearChange} required placeholder='Year' type='text' id='year' name='year' className='form-control' />
                                        <label htmlFor='year'>Year...</label>
                                    </div>
                                </div>

                                <div className='mb-3'>
                                    <label className='fw-bold' htmlFor='model'>Model</label>
                                    <div className={spinnerClasses} id='loading-model-spinner'>
                                        <div className='spinner-grow text-success' role='status'>
                                            <span className='visually-hidden'>Loading...</span>
                                        </div>
                                    </div>
                                    <select onChange={handleModelChange} name='model' id='model' className={dropdownClasses} required>
                                        <option value=''>Choose a model...</option>
                                        {models.map(model => {
                                            return (
                                                <option key={model.href} value={model.id}>{model.name} </option>
                                            )
                                        })}
                                    </select>
                                </div>

                                <button className='btn btn-lg btn-success'>Create Automobile</button>
                            </form>
                            <div className={messageClasses} id='success-message'>
                                Automobile created successfully! If you would like to create another automobile <a href='/automobiles/create' className='alert-link'>click here. </a>
                                To see the current inventory <a href='/automobiles' className='alert-link'>click here.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'></div>
            </div>
        </div>
    );

}
export default AutomobileForm;
