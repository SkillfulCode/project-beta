import { useEffect, useState } from "react";
import Button from 'react-bootstrap/Button';

function SalesList() {

    const [sales, setSales] = useState([]);

    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }

    useEffect(() => {
        loadSales();
    }, []);

    async function handleDeleteSale(href) {
        const saleUrl = `http://localhost:8090${href}`;
        const fetchOptions = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const salesResponse = await fetch(saleUrl, fetchOptions);
        if (salesResponse.ok) {
            loadSales();
        }
    };
    return(
        <div className='container my-4'>
            <h1 className='display-5 fw-bold'>Sales</h1>
            <table className='table table-striped'>
                <thead>
                    <tr className='bg-success'>
                        <th className='text-white text-center'>Automobile</th>
                        <th className='text-white text-center'>Salesperson</th>
                        <th className='text-white text-center'>Customer</th>
                        <th className='text-white text-center'>Price</th>
                        <th className='text-white text-center'>Remove Sale</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.map(sale => {
                        return (
                            <tr key={sale.href} value={sale.href}>
                                <td className='table-success text-center fw-bold'>{sale.automobile.vin}</td>
                                <td className='table-success text-center fw-bold'>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td className='table-success text-center fw-bold'>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td className='table-success text-center fw-bold'>{sale.price}</td>
                                <td className='table-success text-center fw-bold'>
                                    <div className='d-grid gap-4 d-flex mx-4 justify-content-md-center'>
                                    <Button className="btn w-50 btn-danger fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleDeleteSale(sale.href)} variant="outline-dark">Delete</Button>

                                    </div>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );

}
export default SalesList;
