import React, {useEffect, useState } from 'react';

function AppointmentsList() {

  const [appointments, setAppointments] = useState([]);
  const [vin, setVin] = useState('');
  const [selectedVin, setSelectedVin] = useState('');

  async function loadAppointments(vin) {
    const response = await fetch('http://localhost:8080/api/appointments/');
    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  }

  useEffect(() => {
    loadAppointments();
  }, []);

  const handleChangeVin = (event) => {
    const value = event.target.value;
    setVin(value);
  }

  function search() {
    setSelectedVin(vin);
  }

    return (
      <div className="container my-4">
        <h1 className="display-5 fw-bold">Service History</h1>

        <form className="d-flex my-4" onSubmit={e => e.preventDefault()}>
            <input onChange={handleChangeVin} value={vin} className="form-control form-control-md border-success" type="search" placeholder="Search by VIN..." aria-label="Search"/>
            <button className="btn btn-outline-success" onClick={() => search()}>Search</button>
        </form>

        <table className="table table-striped">
          <thead>
            <tr className="bg-success">
              <th className="text-white text-center">VIN</th>
              <th className="text-white text-center">Is VIP?</th>
              <th className="text-white text-center">Customer</th>
              <th className="text-white text-center">Date</th>
              <th className="text-white text-center">Time</th>
              <th className="text-white text-center">Technician</th>
              <th className="text-white text-center">Reason</th>
              <th className="text-white text-center">Status</th>
            </tr>
          </thead>
          <tbody>
          {appointments?.filter(appointment => selectedVin ? appointment.vin === selectedVin : appointment).map(appointment => {
              let vip_status = "No";
              if (appointment.vip) {vip_status = "Yes"};
              return (
                <tr key={appointment.href} value={appointment.href}>
                  <td className="table-success text-center fw-bold">{ appointment.vin }</td>
                  <td className="table-success text-center fw-bold">{ vip_status }</td>
                  <td className="table-success text-center fw-bold">{ appointment.customer }</td>
                  <td className="table-success text-center fw-bold">{ new Date(appointment.date_time).toLocaleDateString() }</td>
                  <td className="table-success text-center fw-bold">{ new Date(appointment.date_time).toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'}) }</td>
                  <td className="table-success text-center fw-bold">{ appointment.technician.first_name } {appointment.technician.last_name}</td>
                  <td className="table-success text-center fw-bold">{ appointment.reason }</td>
                  <td className="table-success text-center fw-bold">{ appointment.status }</td>
                </tr>
              );
            })}
          </tbody>
      </table>
      </div>
    );
  }

  export default AppointmentsList;
