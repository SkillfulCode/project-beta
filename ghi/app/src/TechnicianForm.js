import React, {useState } from 'react';

function TechnicianForm() {

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeId, setEmployeeId] = useState('');
  const [hasSignedUp, setHasSignedUp] = useState(false);
  const [badReq, setBadReq] = useState(false);
  const [message, setMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.employee_id = employeeId;

    const technicianUrl = 'http://localhost:8080/api/technicians/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };


    const technicianResponse = await fetch(technicianUrl, fetchOptions);

    if (technicianResponse.ok) {
      setFirstName('');
      setLastName('');
      setEmployeeId('');
      setHasSignedUp(true);
      setBadReq(false);
    } else {
      const json = await technicianResponse.json();
      setMessage(json.message);
      setBadReq(true);
    }
  }

  const handleChangeFirstName = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleChangeLastName = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleChangeEmployeeId = (event) => {
    const value = event.target.value;
    setEmployeeId(value);
  }

  let badReqClasses = "alert alert-danger d-none";
  if (badReq) {
    badReqClasses = "alert alert-danger";
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col"></div>
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-technician-form">

              <div className={badReqClasses} id="fail-message">{message}</div>

                <h1 className="card-title">Add a Technician</h1>
                <div className="col">
                <label className="fw-bold" htmlFor="first name">First Name</label>
                    <div className="form-group mb-3">
                      <input onChange={handleChangeFirstName} required type="text" id="first_name" name="first_name" className="form-control border-success" />
                    </div>
                  </div>

                  <div className="col">
                  <label className="fw-bold" htmlFor="last name">Last Name</label>
                    <div className="form-group mb-3">
                      <input onChange={handleChangeLastName} required type="text" id="last_name" name="last_name" className="form-control border-success" />
                    </div>
                  </div>

                  <div className="col">
                  <label className="fw-bold" htmlFor="employee_id">Employee ID</label>
                    <div className="form-group mb-3">
                      <input onChange={handleChangeEmployeeId} required type="text" id="employee_id" name="employee_id" className="form-control border-success" />
                    </div>
                  </div>
                <button className="btn btn-lg btn-success">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Welcome! You're all set up! If you'd like to create another technician <a href="/technicians/create" className="alert-link">click here.</a>
              </div>
            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

export default TechnicianForm;
