import React, {useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';

function ManufacturersList() {

  const [manufacturers, setManufacturers] = useState([]);

  async function loadManufacturers() {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  }

  useEffect(() => {
    loadManufacturers();
  }, []);

  async function handleDeleteManufacturer(href) {
    const manufacturerUrl = `http://localhost:8100${href}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
    if (manufacturerResponse.ok) {
      loadManufacturers();
    }
  };

  function handleUpdateManufacturer() {

  };

    return (
      <div className="container my-4">
      <div className="row">
      <div className="col"></div>

      <div className="col-6">
        <h1 className="display-5 fw-bold">Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr className="bg-success">
              <th className="text-white text-center">Name</th>
              <th className="text-white text-center">Manage Manufacturers</th>
            </tr>
          </thead>
          <tbody>
            {manufacturers && manufacturers.map(manufacturer => {
              return (
                <tr key={manufacturer.href} value={manufacturer.href}>
                  <td className="table-success text-center fw-bold w-50">{ manufacturer.name }</td>
                  <td className="table-success text-center fw-bold w-50">
                  <div className="d-grid gap-4 d-flex mx-4 justify-content-md-center">
                    <Button className="btn w-50 btn-warning fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleUpdateManufacturer(manufacturer.href)} variant="outline-dark">Update</Button>
                    <Button className="btn w-50 btn-danger fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleDeleteManufacturer(manufacturer.href)} variant="outline-dark">Delete</Button>
                  </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>

      <div className="col"></div>
      </div>
      </div>
    );
  }

  export default ManufacturersList;
