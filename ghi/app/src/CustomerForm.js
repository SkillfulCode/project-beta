import React, {useState} from 'react'

function CustomerForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');
    const [hasSignedUp, setHasSignedUp] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const cutomerUrl = 'http://localhost:8090/api/customers/';
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const customerResponse = await fetch(cutomerUrl, fetchOptions);
        if (customerResponse.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
            setHasSignedUp(true);
        }
    }

    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return(
        <div className='my-5 container'>
            <div className='row'>
                <div className='col'></div>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>
                            <form className={formClasses} onSubmit={handleSubmit} id='create-customer-form'>
                                <h1 className='card-title'>Add a customer</h1>
                                <div className='col'>
                                <label className='fw-bold' htmlFor='first name'>First Name</label>
                                        <div className='form-floating mb-3'>
                                            <input onChange={handleFirstNameChange} required placeholder='First name' type='text' id='first_name' name='first_name' className='form-control' />
                                        </div>
                                </div>

                                <div className='col'>
                                <label className='fw-bold' htmlFor='last name'>Last Name</label>
                                        <div className='form-floating mb-3'>
                                            <input onChange={handleLastNameChange} required placeholder='Last Name' type='text' id='last_name' name='last_name' className='form-control' />
                                        </div>
                                </div>

                                <div className='col'>
                                <label className='fw-bold' htmlFor='address'>Address</label>
                                        <div className='form-floating mb-3'>
                                            <input onChange={handleAddressChange} required placeholder='Address' type='text' id='address' name='address' className='form-control' />
                                        </div>
                                </div>

                                <div className='col'>
                                <label className='fw-bold' htmlFor='phone number'>Phone Number</label>
                                        <div className='form-floating mb-3'>
                                            <input onChange={handlePhoneNumberChange} required placeholder='Phone number' type='text' id='phone_number' name='phone_number' className='form-control' />
                                        </div>
                                </div>

                                <button className='btn btn-lg btn-success'>Create Customer</button>
                            </form>
                            <div className={messageClasses} id='success-message'>
                                Customer created successfully! If you would like to create another customer <a href='/customers/create' className='alert-link'>click here. </a>
                                To see the customer list <a href='/customers' className='alert-link'>click here.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'></div>
            </div>
        </div>
    );

}
export default CustomerForm;
