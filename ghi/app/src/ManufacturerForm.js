import React, { useState } from 'react';

function ManufacturerForm() {

  const [name, setName] = useState('');
  const [hasSignedUp, setHasSignedUp] = useState(false);

  const [badReq, setBadReq] = useState(false);
  const [message, setMessage] = useState("");

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name;

    const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
    const fetchOptions = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const manufacturerResponse = await fetch(manufacturerUrl, fetchOptions);
    if (manufacturerResponse.ok) {
      setName('');
      setHasSignedUp(true);
      setBadReq(false);
    } else {
      const json = await manufacturerResponse.json();
      setMessage(json.message);
      setBadReq(true);
    }
  }

  const handleChangeName = (event) => {
    const value = event.target.value;
    setName(value);
  }

  let badReqClasses = "alert alert-danger d-none";
  if (badReq) {
    badReqClasses = "alert alert-danger";
  }

  let messageClasses = 'alert alert-success d-none mb-0';
  let formClasses = '';
  if (hasSignedUp) {
    messageClasses = 'alert alert-success mb-0';
    formClasses = 'd-none';
  }

  return (
    <div className="my-5 container">
      <div className="row">
        <div className="col"></div>
        <div className="col-5">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-manufacturer-form">

              <div className={badReqClasses} id="fail-message">{message}</div>

                <h1 className="card-title">Create a Manufacturer</h1>
                  <div className="col">
                  <label className="fw-bold" htmlFor="manufacturer name">Manufacturer Name</label>
                    <div className="form-group mb-3">
                      <input onChange={handleChangeName} required type="text" id="name" name="name" className="form-control border-success" />
                    </div>
                  </div>

                <button className="btn btn-lg btn-success">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Manufacturer created successfully! If you'd like to create another manufacturer <a href="/manufacturers/create" className="alert-link">click here.</a>
              </div>

            </div>
          </div>
        </div>
        <div className="col"></div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
