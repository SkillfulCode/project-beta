import React, {useEffect, useState } from 'react';
import Button from 'react-bootstrap/Button';

function ModelsList() {

  const [models, setModels] = useState([]);

  async function loadModels() {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    loadModels();
  }, []);

  async function handleDeleteModel(href) {
    const modelUrl = `http://localhost:8100${href}`;
    const fetchOptions = {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json',
      },
    }
    const modelResponse = await fetch(modelUrl, fetchOptions);
    if (modelResponse.ok) {
      loadModels();
    }
  };

  function handleUpdateModel() {

  };

    return (
      <div className="container my-4">
        <h1 className="display-5 fw-bold">Models</h1>
        <table className="table table-striped">
          <thead>
            <tr className="bg-success">
              <th className="text-white text-center">Name</th>
              <th className="text-white text-center">Manufacturer</th>
              <th className="text-white text-center">Picture</th>
              <th className="text-white text-center">Manage Models</th>
            </tr>
          </thead>
          <tbody>
            {models && models.map(model => {
              return (
                <tr key={model.href} value={model.href}>
                  <td className="table-success text-center fw-bold">{ model.name }</td>
                  <td className="table-success text-center fw-bold">{ model.manufacturer.name }</td>
                  <td className="table-success text-center fw-bold"><img alt="vehicle" src={model.picture_url}/>
                  </td>
                  <td className="table-success text-center fw-bold">
                  <div className="d-grid gap-4 d-flex mx-4 justify-content-md-center">
                    <Button className="btn w-50 btn-warning fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleUpdateModel(model.href)} variant="outline-dark">Update</Button>
                    <Button className="btn w-50 btn-danger fw-bold btn-sm d-none d-md-block text-light shadow-sm" onClick={() => handleDeleteModel(model.href)} variant="outline-dark">Delete</Button>
                  </div>
                  </td>
                </tr>
              );
            })}
          </tbody>
      </table>
      </div>
    );
  }

  export default ModelsList;
