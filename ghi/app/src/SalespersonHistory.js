import React, {useEffect, useState} from "react";
function SalespersonHistory() {
    const [sales, setSales] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [salespeople, setSalespeople] = useState([]);

    async function loadSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales);
        }
    }
    useEffect(() => {
        loadSales();
    }, []);
    async function loadSalespeople() {
        const response = await fetch('http://localhost:8090/api/salespeople/');
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }
    useEffect(() => {
        loadSalespeople();
    }, []);
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);

    }
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (sales.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select border-success';
    }
    return(
        <div className='container my-4'>
            <h1 className='display-5 fw-bold'>Sales History</h1>
            <div className='mb-3'>
                <label className='fw-bold' htmlFor='salesperson'>Salesperson</label>
                <div className={spinnerClasses} id='loading-salesperson-spinner'>
                    <div className='spinner-grow text-success' role='status'>
                        <span className='visually-hidden'>Loading...</span>
                    </div>
                </div>
                <select onChange={handleSalespersonChange} name='salesperson' id='salesperson' className={dropdownClasses} required>
                    <option value=''>Choose a salesperson...</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.href} value={salesperson.href}>{salesperson.first_name} {salesperson.last_name}</option>
                            )
                    })}
                </select>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr className='bg-success'>
                        <th className='text-white text-center'>Salesperson</th>
                        <th className='text-white text-center'>Customer</th>
                        <th className='text-white text-center'>VIN</th>
                        <th className='text-white text-center'>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.filter(
                      sale => sale.salesperson.href.includes(salesperson)
                        ).map(sale => {
                       return (
                        <tr key={sale.href} value={sale.href}>
                            <td className='table-success text-center fw-bold'>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                            <td className='table-success text-center fw-bold'>{sale.customer.first_name} {sale.customer.last_name}</td>
                            <td className='table-success text-center fw-bold'>{sale.automobile.vin}</td>
                            <td className='table-success text-center fw-bold'>{sale.price}</td>
                        </tr>
                       );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default SalespersonHistory;
