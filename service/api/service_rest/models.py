from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=150, unique=True)

    def get_api_url(self):
        return reverse("api_show_technician", kwargs={"id": self.id})

    def __str__(self):
        return self.employee_id

    class Meta:
        ordering = ("employee_id",)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=8)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=150)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )
    vip = models.BooleanField(default=False)

    def finish(self):
        status = "finished"
        self.status = status
        self.save()

    def cancel(self):
        status = "canceled"
        self.status = status
        self.save()

    def vip_status(self):
        status = True
        self.vip = status
        self.save()

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"id": self.id})

    def __str__(self):
        return self.customer

    class Meta:
        ordering = ("customer",)
