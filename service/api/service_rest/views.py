from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Technician, Appointment, AutomobileVO
from django.views.decorators.http import require_http_methods
import json
from django.db import IntegrityError


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "employee_id",
        "first_name",
        "last_name",
    ]


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            if content["employee_id"] == "":
                return JsonResponse(
                    {"message": "Employee ID cannot be empty."},
                    status=400,
                )
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except (KeyError, IntegrityError, ValueError) as e:
            return JsonResponse(
                {"message": f"{e}"},
                status=400,
            )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_technician(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician ID."},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Technician.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"deleted": True})
        else:
            return JsonResponse(
                {"message": "Invalid technician ID."},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            if content["employee_id"] == "":
                return JsonResponse(
                    {"message": "Employee ID cannot be empty."},
                    status=400,
                )
            Technician.objects.filter(id=id).update(**content)
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except (Technician.DoesNotExist, KeyError, IntegrityError, ValueError) as e:
            return JsonResponse(
                {"message": f"{e}"},
                status=400,
            )


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            vin = content["vin"]
            AutomobileVO.objects.get(vin=vin)
            content["vip"] = True
        except AutomobileVO.DoesNotExist:
            pass

        try:
            employee_id = content["technician"]
            technician = Technician.objects.get(employee_id=employee_id)
            content["technician"] = technician
            content["status"] = "upcoming"
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except (Technician.DoesNotExist, KeyError, IntegrityError, ValueError) as e:
            return JsonResponse(
                {"message": f"{e}"},
                status=400,
            )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid appointment Id."},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Appointment.objects.filter(id=id).delete()
        if count > 0:
            return JsonResponse({"deleted": count > 0})
        else:
            return JsonResponse(
                {"message": "Invalid appointment ID."},
                status=400,
            )
    else:
        try:
            content = json.loads(request.body)
            Appointment.objects.filter(id=id).update(**content)
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except (Appointment.DoesNotExist, KeyError, IntegrityError, ValueError) as e:
            return JsonResponse(
                {"message": f"{e}"},
                status=400,
            )


@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.finish()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid appointment ID."},
            status=400,
        )


@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.cancel()
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid appointment ID."},
            status=400,
        )
